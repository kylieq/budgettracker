import sqlite3

def sql_connection():
    try:
        connection = sqlite3.connect('budget.db')
        return connection
    except Error:
        print(Error)

# Start New Session - Overwrite tables if they already exist
def create_new_table(connection):
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS Income")
    cursor.execute("DROP TABLE IF EXISTS Saving")
    cursor.execute("DROP TABLE IF EXISTS Housing")
    cursor.execute("DROP TABLE IF EXISTS Food")
    cursor.execute("DROP TABLE IF EXISTS Misc")
    cursor.execute("DROP TABLE IF EXISTS Personal")
    # Must check if table exists or not
    cursor.execute("CREATE TABLE Income(source text, amount decimal)")
    cursor.execute("CREATE TABLE Saving(fund text, amount decimal)")
    cursor.execute("CREATE TABLE Housing(bill text, amount decimal)")
    cursor.execute("CREATE TABLE Food(category text, amount decimal)")
    cursor.execute("CREATE TABLE Misc(category text, amount decimal)")
    cursor.execute("CREATE TABLE Personal(category text, amount decimal)")
    connection.commit()

# Resume Session
def continue_old_table(connection):
    cursor = connection.cursor()
    # Must check if table exists or not
    cursor.execute("CREATE TABLE IF NOT EXISTS Income(source text, amount decimal)")
    cursor.execute("CREATE TABLE IF NOT EXISTS Saving(fund text, amount decimal)")
    cursor.execute("CREATE TABLE IF NOT EXISTS Housing(bill text, amount decimal)")
    cursor.execute("CREATE TABLE IF NOT EXISTS Food(category text, amount decimal)")
    cursor.execute("CREATE TABLE IF NOT EXISTS Misc(category text, amount decimal)")
    cursor.execute("CREATE TABLE IF NOT EXISTS Personal(category text, amount decimal)")
    connection.commit()

def check_budget(category, amount, connection):
    housing = sum_data_for("housing", connection)
    food = sum_data_for("food", connection)
    misc = sum_data_for("misc", connection)

    income = sum_data_for("income", connection)
    needs = housing + food + misc # 50%
    wants = sum_data_for("personal", connection) # 30%
    savings = sum_data_for("saving", connection) # 20%

    needs_max = float(income * 0.5)
    sum_needs = needs + amount
    excess_needs = sum_needs - needs_max

    wants_max = float(income * 0.3)
    sum_wants = wants + amount
    excess_wants = wants - wants_max

    savings_max = float(income * 0.2)
    sum_savings = savings + amount
    excess_savings = savings - savings_max

    if category == "needs":
        if sum_needs > needs_max:
            print("> Warning:")
            print("> ...Amount spent on needs exceeds the expected total.")
            print("> ...Limit expenses in other categories to accommodate this increase in needs.") 
            print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_needs, 2)))
    elif category == "wants":
        if sum_wants > wants_max:
            print("> Warning:")
            print("> ...Amount spent on wants exceeds the expected total.")
            if needs < needs_max:
                print("> ...However, less money was spent on needs than expected.")
                print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))
            else:
                print("> ...Reduce expenses made in this category.")    
                print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_wants, 2)))
    else:
        if sum_savings > savings_max:
            print("> Warning:")
            print("> ...Amount spent on wants exceeds the expected total.")
            if needs < needs_max:
                print("> ...However, less money was spent on needs than expected.")
                print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))
            else:
                print("> ...Amount reserved for savings exceeds the expected total.")   
                print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_savings, 2)))

# Insert in Table
def insert(category, connection, entities):
    cursor = connection.cursor()
    if category == "income":
        cursor.execute('INSERT INTO Income(source, amount) VALUES(?, ?)', entities)
    elif category == "saving":
        check_budget("savings", float(entities[1]), connection)
        cursor.execute('INSERT INTO Saving(fund, amount) VALUES(?, ?)', entities)
    elif category == "housing":
        check_budget("needs", float(entities[1]), connection)
        cursor.execute('INSERT INTO Housing(bill, amount) VALUES(?, ?)', entities)
    elif category == "food":
        check_budget("needs", float(entities[1]), connection)
        cursor.execute('INSERT INTO Food(category, amount) VALUES(?, ?)', entities)
    elif category == "miscellaneous":
        check_budget("needs", float(entities[1]), connection)
        cursor.execute('INSERT INTO Misc(category, amount) VALUES(?, ?)', entities)
    elif category == "personal":
        check_budget("wants", float(entities[1]), connection)
        cursor.execute('INSERT INTO Personal(category, amount) VALUES(?, ?)', entities)
    connection.commit()

# Get sum of data
def sum_data_for(category, connection):
    cursor = connection.cursor()
    total = 0
    if category == "income":
        cursor.execute('SELECT SUM(amount) FROM Income')
        total = cursor.fetchall()[0][0]
    if category == "saving":
        cursor.execute('SELECT SUM(amount) FROM Saving')
        total = cursor.fetchall()[0][0]
    elif category == "housing":
        cursor.execute('SELECT SUM(amount) FROM Housing')
        total = cursor.fetchall()[0][0]
    elif category == "food":
        cursor.execute('SELECT SUM(amount) FROM Food')
        total = cursor.fetchall()[0][0]
    elif category == "miscellaneous":
        cursor.execute('SELECT SUM(amount) FROM Misc')
        total = cursor.fetchall()[0][0]
    elif category == "personal":
        cursor.execute('SELECT SUM(amount) FROM Personal')
        total = cursor.fetchall()[0][0]
    connection.commit()
    return round(total, 2)

# Update table
def update(category, connection, source_name, new_value):
    cursor = connection.cursor()
    if category == "income":
        sql = '''UPDATE Income SET amount = ? WHERE source = ?'''
        cursor.execute(sql, (new_value, source_name))
    elif category == "saving":
        check_budget("savings", float(new_value), connection)
        sql = '''UPDATE Saving SET amount = ? WHERE fund = ?'''
        cursor.execute(sql, (new_value, source_name))
    elif category == "housing":
        check_budget("needs", float(new_value), connection)
        sql = '''UPDATE Housing SET amount = ? WHERE bill = ?'''
        cursor.execute(sql, (new_value, source_name))
    elif category == "food":
        check_budget("needs", float(new_value), connection)
        sql = '''UPDATE Food SET amount = ? WHERE category = ?'''
        cursor.execute(sql, (new_value, source_name))
    elif category == "miscellaneous":
        check_budget("needs", float(new_value), connection)
        sql = '''UPDATE Misc SET amount = ? WHERE category = ?'''
        cursor.execute(sql, (new_value, source_name))
    elif category == "personal":
        check_budget("wants", float(new_value), connection)
        sql = '''UPDATE Personal SET amount = ? WHERE category = ?'''
        cursor.execute(sql, (new_value, source_name))
    connection.commit()

# Fetch all data
def display_all(select, connection):
    cursor = connection.cursor()
    if select == "all":
        cursor.execute('SELECT * FROM Income')
        rows = cursor.fetchall()
        print("> INCOME")
        for row in rows:
            print("> ...{}: ${}".format(row[0], row[1]))

    cursor.execute('SELECT * FROM Saving')
    rows = cursor.fetchall()
    print("> SAVING")
    for row in rows:
        print("> ...{}: ${}".format(row[0], row[1]))

    cursor.execute('SELECT * FROM Housing')
    rows = cursor.fetchall()
    print("> HOUSING EXPENSES")
    for row in rows:
        print("> ...{}: ${}".format(row[0], row[1]))

    cursor.execute('SELECT * FROM Food')
    rows = cursor.fetchall()
    print("> FOOD EXPENSES")
    for row in rows:
        print("> ...{}: ${}".format(row[0], row[1]))

    cursor.execute('SELECT * FROM Misc')
    rows = cursor.fetchall()
    print("> MISCELLANEOUS EXPENSES")
    for row in rows:
        print("> ...{}: ${}".format(row[0], row[1]))

    cursor.execute('SELECT * FROM Personal')
    rows = cursor.fetchall()
    print("> PERSONAL EXPENSES")
    for row in rows:
        print("> ...{}: ${}".format(row[0], row[1]))

def fetch_all_from(category, connection):
    cursor = connection.cursor()
    if category == "income":
        print("> INCOME")
        cursor.execute('SELECT * FROM Income')
    elif category == "saving":
        print("> SAVING")
        cursor.execute('SELECT * FROM Saving')
    elif category == "housing":
        print("> HOUSING EXPENSES")
        cursor.execute('SELECT * FROM Housing')
    elif category == "food":
        print("> FOOD EXPENSES")
        cursor.execute('SELECT * FROM Food')
    elif category == "miscellaneous":
        print("> MISCELLANEOUS EXPENSES")
        cursor.execute('SELECT * FROM Misc')
    elif category == "personal":
        print("> PERSONAL EXPENSES")
        cursor.execute('SELECT * FROM Personal')

    rows = cursor.fetchall()
    for row in rows:    
        print("> ...{}: ${}".format(row[0], (row[1])))