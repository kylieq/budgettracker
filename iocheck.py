from sqlite3 import *
import sqlfunc

def budget_data(category, connection):
	housing = sqlfunc.sum_data_for("housing", connection)
	food = sqlfunc.sum_data_for("food", connection)
	misc = sqlfunc.sum_data_for("misc", connection)

	income = sqlfunc.sum_data_for("income", connection)
	needs = housing + food + misc # 50%
	wants = sqlfunc.sum_data_for("personal", connection) # 30%
	savings = sqlfunc.sum_data_for("saving", connection) # 20%

	needs_max = float(income * 0.5)
	excess_needs = needs - needs_max

	wants_max = float(income * 0.3)
	excess_wants = wants - wants_max

	savings_max = float(income * 0.2)
	excess_savings = savings - savings_max

	if category == "needs":
		print("> NEEDS SUMMARY")
		if needs > needs_max:
			print("> ...Amount spent on needs exceeds the expected total.")
			print("> ...Limit expenses in other categories to accommodate this increase in needs.") 
			print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_needs, 2)))
		else:
			print("> ...Amount spent on needs totals no more than 50% of your monthly income.")
	elif category == "wants":
		print("> WANTS SUMMARY")
		if wants > wants_max:
			print("> ...Amount spent on wants exceeds the expected total.")
			if needs < needs_max:
				print("> ...However, less money was spent on needs than expected.")
				print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))
			else:
				print("> ...Reduce expenses made in this category.")    
				print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_wants, 2)))
		else:
			print("> ...Amount spent on wants totals no more than 30% of your monthly income.")
	else:
		print("> SAVINGS SUMMARY")
		if savings > savings_max:
			print("> ...Amount reserved for savings exceeds limit.")
			print("> ...Exceeded limit by: ${0:.2f}".format(round(excess_savings, 2)))
			if needs < needs_max:
				print("> ...However, less money was spent on needs than expected.")
				print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))
		else:
			print("> ...Amount reserved for savings totals no more than 20% of your monthly income.")

def income(connection):
	print("> INCOME")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add income")
		print("> ...2 Update income")
		print("> ...3 Display income")
		print("> ...4 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add income")
			print("> ...2 Update income")
			print("> ...3 Display income")
			print("> ...4 Back")
			answer = input("> ")

		if answer == "1":
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter source of income:")
				source = input("> ").title()
				print("> Enter monthly income from '{}':".format(source))
				income = input("> ")

				entities = (source, income)
				sqlfunc.insert("income", connection, entities)

				print("> Are there anymore sources of income you would like to add? (y or n)")
				if input("> ") == "y":
					continue
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current income logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Income')
			income_li = []
			rows = cursor.fetchall()
			for row in rows:
				income_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which field would you like to update income?")
			source_name = input("> ").title()
			while source_name not in income_li:
				print("> Invalid field. Please try again.")
				print("> For which field would you like to update income?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_income = input("> ")
			sqlfunc.update("income", connection, source_name, float(new_income))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("income", connection)
		elif answer == "4":
			EXIT1 = 1

def saving(connection):
	print("> SAVING")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add funds")
		print("> ...2 Update funds")
		print("> ...3 Display funds")
		print("> ...4 Summary")
		print("> ...5 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add funds")
			print("> ...2 Update funds")
			print("> ...3 Display funds")
			print("> ...4 Summary")
			print("> ...5 Back")
			answer = input("> ")

		if answer == "1":	
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter which fund this is being saved to:")
				service = input("> ").title()
				print("> Enter monthly amount saved for '{}':".format(service))
				amount = input("> ")

				entities = (service, amount)
				sqlfunc.insert("saving", connection, entities)

				print("> Are there anymore savings you would like to add? (y or n)")
				if input("> ") == "y":
					continue
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current savings logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Saving')
			expenses_li = []
			rows = cursor.fetchall()
			for row in rows:
				expenses_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which fund would you like to update savings?")
			source_name = input("> ").title()
			while source_name not in expenses_li:
				print("> Invalid field. Please try again.")
				print("> For which fund would you like to update savings?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_amount = input("> ")
			sqlfunc.update("saving", connection, source_name, float(new_amount))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("saving", connection)
		elif answer == "4":
			budget_data("savings", connection)
		elif answer == "5":
			EXIT1 = 1

def housing(connection):
	print("> HOUSING")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add expenses")
		print("> ...2 Update expenses")
		print("> ...3 Display expenses")
		print("> ...4 Summary")
		print("> ...5 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add expenses")
			print("> ...2 Update expenses")
			print("> ...3 Display expenses")
			print("> ...4 Summary")
			print("> ...5 Back")
			answer = input("> ")

		if answer == "1":
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter what service is being paid for:")
				service = input("> ").title()
				print("> Enter amount due for '{}':".format(service))
				amount = input("> ")

				entities = (service, amount)
				sqlfunc.insert("housing", connection, entities)

				print("> Are there anymore housing expenses you would like to add? (y or n)")
				if input("> ") == "y":
					continue

				elif answer == "2":
					sqlfunc.fetch_all_from(connection)
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current expenses logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Housing')
			expenses_li = []
			rows = cursor.fetchall()
			for row in rows:
				expenses_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which field would you like to update an expense?")
			source_name = input("> ").title()
			while source_name not in expenses_li:
				print("> Invalid field. Please try again.")
				print("> For which field would you like to update an expense?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_amount = input("> ")
			sqlfunc.update("housing", connection, source_name, float(new_amount))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("housing", connection)
		elif answer == "4":
			budget_data("needs", connection)
		elif answer == "5":
			EXIT1 = 1

def food(connection):
	print("> FOOD")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add expenses")
		print("> ...2 Update expenses")
		print("> ...3 Display expenses")
		print("> ...4 Summary")
		print("> ...5 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add expenses")
			print("> ...2 Update expenses")
			print("> ...3 Display expenses")
			print("> ...4 Summary")
			print("> ...5 Back")
			answer = input("> ")

		if answer == "1":
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter what is being paid for:")
				service = input("> ").title()
				print("> Enter amount due for '{}':".format(service))
				amount = input("> ")

				entities = (service, amount)
				sqlfunc.insert("food", connection, entities)

				print("> Are there anymore food expenses you would like to add? (y or n)")
				if input("> ") == "y":
					continue

				elif answer == "2":
					sqlfunc.fetch_all_from(connection)
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current expenses logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Food')
			expenses_li = []
			rows = cursor.fetchall()
			for row in rows:
				expenses_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which field would you like to update an expense?")
			source_name = input("> ").title()
			while source_name not in expenses_li:
				print("> Invalid field. Please try again.")
				print("> For which field would you like to update an expense?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_amount = input("> ")
			sqlfunc.update("food", connection, source_name, float(new_amount))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("food", connection)
		elif answer == "4":
			budget_data("needs", connection)
		elif answer == "5":
			EXIT1 = 1

def misc(connection):
	print("> MISCELLANEOUS")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add expenses")
		print("> ...2 Update expenses")
		print("> ...3 Display expenses")
		print("> ...4 Summary")
		print("> ...5 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add expenses")
			print("> ...2 Update expenses")
			print("> ...3 Display expenses")
			print("> ...4 Summary")
			print("> ...5 Back")
			answer = input("> ")

		if answer == "1":
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter what is being paid for:")
				service = input("> ").title()
				print("> Enter amount due for '{}':".format(service))
				amount = input("> ")

				entities = (service, amount)
				sqlfunc.insert("miscellaneous", connection, entities)

				print("> Are there anymore miscellaneous expenses you would like to add? (y or n)")
				if input("> ") == "y":
					continue

				elif answer == "2":
					sqlfunc.fetch_all_from("miscellaneous", connection)
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current expenses logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Misc')
			expenses_li = []
			rows = cursor.fetchall()
			for row in rows:
				expenses_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which field would you like to update an expense?")
			source_name = input("> ").title()
			while source_name not in expenses_li:
				print("> Invalid field. Please try again.")
				print("> For which field would you like to update an expense?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_amount = input("> ")
			sqlfunc.update("miscellaneous", connection, source_name, float(new_amount))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("miscellaneous", connection)
		elif answer == "4":
			budget_data("needs", connection)
		elif answer == "5":
			EXIT1 = 1

def personal(connection):
	print("> PERSONAL")
	EXIT1 = 0
	while (EXIT1 != 1):
		print("> Select one of the following:")
		print("> ...1 Add expenses")
		print("> ...2 Update expenses")
		print("> ...3 Display expenses")
		print("> ...4 Summary")
		print("> ...5 Back")
		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Add expenses")
			print("> ...2 Update expenses")
			print("> ...3 Display expenses")
			print("> ...4 Summary")
			print("> ...5 Back")
			answer = input("> ")

		if answer == "1":
			EXIT2 = 0
			while (EXIT2 != 1):
				print("> Enter what is being paid for:")
				service = input("> ").title()
				print("> Enter amount due for '{}':".format(service))
				amount = input("> ")

				entities = (service, amount)
				sqlfunc.insert("personal", connection, entities)

				print("> Are there anymore personal expenses you would like to add? (y or n)")
				if input("> ") == "y":
					continue

				elif answer == "2":
					sqlfunc.fetch_all_from("personal", connection)
				else:
					EXIT2 = 1

		elif answer == "2":
			print("> Current expenses logged:")
			cursor = connection.cursor()
			cursor.execute('SELECT * FROM Personal')
			expenses_li = []
			rows = cursor.fetchall()
			for row in rows:
				expenses_li.append(row[0])
				print("> {}: ${}".format(row[0], row[1]))

			print("> For which field would you like to update an expense?")
			source_name = input("> ").title()
			while source_name not in expenses_li:
				print("> Invalid field. Please try again.")
				print("> For which field would you like to update an expense?")
				source_name = input("> ").title()

			print("> Enter new amount:")
			new_amount = input("> ")
			sqlfunc.update("personal", connection, source_name, float(new_amount))
			cursor.close()
			EXIT2 = 1

		elif answer == "3":
			sqlfunc.fetch_all_from("personal", connection)
		elif answer == "4":
			budget_data("wants", connection)
		elif answer == "5":
			EXIT1 = 1
