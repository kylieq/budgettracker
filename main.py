from sqlite3 import *
import time
import sqlfunc
import iocheck

def budget_percentages(connection):
	print("> SUMMARY")
	# 50/30/20 budget plan
	housing = sqlfunc.sum_data_for("housing", connection)
	food = sqlfunc.sum_data_for("food", connection)
	misc = sqlfunc.sum_data_for("misc", connection)

	income = sqlfunc.sum_data_for("income", connection)
	needs = housing + food + misc # 50%
	wants = sqlfunc.sum_data_for("personal", connection) # 30%
	savings = sqlfunc.sum_data_for("saving", connection) # 20%

	needs_max = float(income * 0.5)
	needs_percent = float(needs / income) 
	print("> Needs:")
	if needs_percent <= 0.5:
		leftover = needs_max - needs
		print("> ...Amount spent on needs totals no more than 50% of your monthly income.")
		print("> ...You may distribute the following amount among other categories: ${0:.2f}".format(leftover))
	else:
		excess = needs - needs_max
		print("> ...Amount spent on needs exceeds the expected total.")
		print("> ...Limit expenses in other categories to accommodate this increase in needs.")	
		print("> ...Exceeded limit by: ${0:.2f}".format(excess))

	wants_max = float(income * 0.3)
	wants_percent = float(wants / income) 
	print("> Wants:")
	if wants_percent <= 0.3:
		leftover = wants_max - wants
		print("> ...Amount spent on wants totals no more than 30% of your monthly income.")
	else:
		excess = wants - wants_max
		print("> ...Amount spent on wants exceeds the expected total.")
		print("> ...Exceeded limit by: ${0:.2f}".format(excess))
		if needs < needs_max:
			print("> ...However, less money was spent on needs than expected.")
			print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))
		else:
			print("> ...Reduce expenses made in this category.")	

	savings_max = float(income * 0.2)
	savings_percent = float(savings / income) 
	print("> Savings:")
	if savings_percent <= 0.2:
		leftover = savings_max - savings
		print("> ...Amount reserved for savings totals no more than 20% of your monthly income.")
	else:
		excess = savings - savings_max
		print("> ...Amount reserved for savings exceeds the expected total.")	
		print("> ...Exceeded limit by: ${0:.2f}".format(excess))
		if needs < needs_max:
			print("> ...However, less money was spent on needs than expected.")
			print("> ...This leaves ${0:.2f} for personal spending and/or saving.".format(round(needs_max-needs, 2)))

def sum_expenses(connection):
	total_expenses = 0;
	cursor = connection.cursor()

	cursor.execute('SELECT SUM(amount) FROM Saving')
	total_saving = cursor.fetchall()
	total_expenses += total_saving[0][0]

	cursor.execute('SELECT SUM(amount) FROM Housing')
	total_housing = cursor.fetchall()
	total_expenses += total_housing[0][0]

	cursor.execute('SELECT SUM(amount) FROM Food')
	total_food = cursor.fetchall()
	total_expenses += total_food[0][0]

	cursor.execute('SELECT SUM(amount) FROM Misc')
	total_misc = cursor.fetchall()
	total_expenses += total_misc[0][0]

	cursor.execute('SELECT SUM(amount) FROM Personal')
	total_personal = cursor.fetchall()
	total_expenses += total_personal[0][0]

	return round(total_expenses, 2)

# Enter all expenses until you reach zero (Income - Expenses = 0)
def zero_based_budget(connection):
	total_income = sum_income(connection)
	total_expenses = sum_expenses(connection)
	difference = total_income - total_expenses
	return difference

def main():
	connection = sqlfunc.sql_connection()

	print("> Welcome to Budget Tracker\n>")
	time.sleep(1);

	print("> Select one of the following:")
	print("> ...1 Start New Session")
	print("> ...2 Resume Previous Session")
	print("> ...3 Quit")
	answer = input("> ")

	while answer not in ("1", "2", "3"):
		print("> Invalid input. Please try again.")
		print("> Select one of the following:")
		print("> ...1 Start New Session")
		print("> ...2 Resume Previous Session")
		print("> ...3 Quit")
		answer = input("> ")

	if answer == "1":
		sqlfunc.create_new_table(connection)
	elif answer == "2":
		sqlfunc.continue_old_table(connection)
	else: 
		return 1

	while 1:
		print("> Select one of the following:")
		print("> ...1 Income")
		print("> ...2 Expenses")
		print("> ...3 Display All")
		print("> ...4 Summary")
		print("> ...5 Quit")

		answer = input("> ")

		while answer not in ("1", "2", "3", "4", "5"):
			print("> Invalid input. Please try again.")
			print("> Select one of the following:")
			print("> ...1 Income")
			print("> ...2 Expenses")
			print("> ...3 Display All")
			print("> ...4 Summary")
			print("> ...5 Quit")
			answer = input("> ")

		if answer == "1":
			iocheck.income(connection)
		elif answer == "2":
			EXIT = 0
			while EXIT != 1:
				print("> Select one of the following:")
				print("> ...1 Saving")
				print("> ...2 Housing")
				print("> ...3 Food")
				print("> ...4 Miscellaneous")
				print("> ...5 Personal")
				print("> ...6 Display All")
				print("> ...7 Back")
				answer = input("> ")

				while answer not in ("1", "2", "3", "4", "5", "6", "7"):
					print("> Select one of the following:")
					print("> ...1 Saving")
					print("> ...2 Housing")
					print("> ...3 Food")
					print("> ...4 Miscellaneous")
					print("> ...5 Personal")
					print("> ...6 Display All")
					print("> ...7 Back")
					answer = input("> ")
					
				if answer == "1":
					iocheck.saving(connection)
				elif answer == "2":	
					iocheck.housing(connection)
				elif answer == "3":
					iocheck.food(connection)
				elif answer == "4":
					iocheck.misc(connection)
				elif answer == "5":
					iocheck.personal(connection)
				elif answer == "6":
					sqlfunc.display_all("expenses", connection)
				elif answer == "7":
					EXIT = 1

		elif answer == "3":
			income = sqlfunc.sum_data_for("income", connection)
			expenses = sum_expenses(connection)
			print("> Total income: ${0:.2f}".format(income))
			print("> Total expenses: ${0:.2f}".format(expenses))

		elif answer == "4":
			budget_percentages(connection)

		elif answer == "5":
			print("> Exiting...")
			break

	connection.close()
	return 1

if __name__ == "__main__":
	main()